# Lab8 - measure failure intensity

## Introduction

Ok, during the last lab we were doing fuzz testing, this is a good tool to understand which inputs may break your app. But how to understand if you need to quality maitenance, and call an ambulance to save your product. There is the only way to understand it, you need to measure failure intensity of your product, and guess what we are going to do today. ***Let's roll!***

## Failure intensity

Well, this is a time when the name speaks for itself. Failure intensity is an amount of failures on your service during the unit of time(or code). Measuring it will help you to understand if something wrong goes with your service, to check it you may count number of 503/404 and other responses, compared to your normal 200s. We will talk about load testing later, but for now lets keep it simple. 

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab7 - Failure intensity
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab7-measure-failure-intensity-s22)
2. For todays lab you will need Apache Benchmark, on linux you can install it using shell command:
```sh
sudo apt install apache2-utils
```
3. And for testing we need some app, just use link from previous lab.
4. To test it you should run command like this one:
```sh
ab -n 20000 -c 100 -m "GET" _url_
```
This means that we are going to send 20000 requests, with 100 of them sending concurrently to the server. We have few lines like `Failed requests` and `Non-2xx responses`, which are exactly failures we're searching here. To measure the exact metric you should use just this formule, where MTTF is time between failures:
`FI = 1 / MTTF`.

## Homework

As a homework you will need to test your service(GetSpec request of InnoDrive from previous labs) and provide the results of your test as a screenshot when failure intensity is equal to the `0.2+-5%`. + provide the calculations for the failure intensity

## Screenshot

![screenshot](screenshot.png)

## Calculations

**Failed requests** = `4843`

**Time** = `24717 ms`

**MTTF** = 4843 / 24717 = `5.10365477`

**FI** = 1 / **MTTF** = 1 / 5.10365477 = `0.195938018` ~= 0.2 (+- 5 %)